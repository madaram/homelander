package homelander.drones.tests;

import homelander.drones.batch.Application;
import homelander.drones.command.Command;
import homelander.drones.exceptions.DroneParseException;
import homelander.drones.exceptions.MaximumSizeException;
import homelander.drones.position.Position;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class CartesianCoordinatePositionTest {

	@Autowired
	@Qualifier("cartesianCoordinatePosition")
	private Position positionToTest;

	@Test(expected = DroneParseException.class)
	public void testWrongStringPosition() throws DroneParseException {
		positionToTest.init("0 0 a");
	}

	@Test
	public void testGoForward() throws DroneParseException {
		positionToTest.init("0 0 N");
		positionToTest.changePosition(Command.GO_FORWARD);
		Assert.assertEquals("0 1 N", positionToTest.toString());
	}

	@Test
	public void testParsePosition() throws DroneParseException {
		positionToTest.init("0 0 N");
		Assert.assertEquals("0 0 N", positionToTest.toString());
	}

	@Test
	public void testSmallTour() throws DroneParseException {
		positionToTest.init("0 0 N");
		positionToTest.changePosition(Command.GO_FORWARD);
		positionToTest.changePosition(Command.TURN_RIGHT);
		positionToTest.changePosition(Command.TURN_RIGHT);
		positionToTest.changePosition(Command.GO_FORWARD);
		positionToTest.changePosition(Command.TURN_RIGHT);
		positionToTest.changePosition(Command.TURN_RIGHT);
		Assert.assertEquals("0 0 N", positionToTest.toString());
	}

	@Test
	public void testBoundaries() throws DroneParseException {
		positionToTest.init("0 0 S");
		positionToTest.changePosition(Command.GO_FORWARD);
		positionToTest.changePosition(Command.GO_FORWARD);
		Assert.assertEquals("0 0 S", positionToTest.toString());
	}

	@Test
	public void testBoundariesMaximum() throws DroneParseException {
		positionToTest.setBoundaries("100 100");
		positionToTest.init("100 100 E");
		positionToTest.changePosition(Command.GO_FORWARD);
		Assert.assertEquals("100 100 E", positionToTest.toString());
		positionToTest.changePosition(Command.TURN_LEFT);
		positionToTest.changePosition(Command.GO_FORWARD);
		Assert.assertEquals("100 100 N", positionToTest.toString());
	}

	@Test
	public void testTurnLeft() throws DroneParseException {
		positionToTest.init("0 0 N");
		positionToTest.changePosition(Command.TURN_LEFT);
		Assert.assertEquals("0 0 O", positionToTest.toString());
	}

	@Test
	public void testTurnRight() throws DroneParseException {
		positionToTest.init("0 0 N");
		positionToTest.changePosition(Command.TURN_RIGHT);
		Assert.assertEquals("0 0 E", positionToTest.toString());
	}

	@Test(expected = MaximumSizeException.class)
	public void testLimitsXaxis() throws DroneParseException,
			MaximumSizeException {
		positionToTest.setBoundaries("151 150");
	}

	@Test(expected = MaximumSizeException.class)
	public void testLimitsYaxis() throws DroneParseException,
			MaximumSizeException {
		positionToTest.setBoundaries("150 201");
	}

}