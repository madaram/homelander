package homelander.drones.tests;

import homelander.drones.batch.Application;
import homelander.drones.exceptions.MaximumDroneNumberException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class ApplicationTest {

	private static final int TIMEOUT_MS = 1000000;

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

	@Before
	public void setUpStreams() {
		System.setOut(new PrintStream(outContent));
	}

	@After
	public void cleanUpStreams() {
		System.setOut(null);
		System.setErr(null);
	}

	@Test(timeout = TIMEOUT_MS)
	public void testApplicationWithNoInputFile() {
		String[] args = null;
		Application.main(args);
		Assert.assertEquals("You must specify an input file.", outContent
				.toString().trim().split("\n")[0].trim());
	}

	@Test(timeout = TIMEOUT_MS)
	public void testDroneWithLongCommands() {
		String[] args = { "-i", "src/test/resources/inputLongCommand.csv" };
		Application.main(args);
		Assert.assertEquals(
				"Error while parsing file. You cannot have a command of more than 100 characters. See drone DRONE_FAIL",
				outContent.toString().trim());
	}

	@Test(timeout = TIMEOUT_MS)
	public void testDroneUnexistingFile() {
		String fileName = "Idonotexist";
		String[] args = { "-i", fileName };
		Application.main(args);
		outContent.toString().trim().contains("Error while reading file");
		Assert.assertTrue(outContent.toString().trim()
				.contains("Error while reading file"));
		// Assert.assertEquals(
		// "Error while reading file : Idonotexist (Le fichier spécifié est introuvable)",
		// outContent.toString().trim());
	}

	@Test(timeout = TIMEOUT_MS)
	public void testDroneMoreThanTenDrones() {
		String[] args = { "-i", "src/test/resources/inputLongTooManyDrones.csv" };
		Application.main(args);
		Assert.assertEquals(String.format(
				"Error : you defined more than %s drones.",
				MaximumDroneNumberException.MAXIMUM_DRONE_NUMBER), outContent
				.toString().trim());
	}

	@Test(timeout = TIMEOUT_MS)
	public void testFullCorrectExemple() throws IOException {
		String[] args = { "-i", "src/test/resources/input.csv", "-t" };
		Application.main(args);
		// Assert.assertEquals(
		// FileUtils
		// .readFileToString(
		// new File(
		// "src/test/resources/expectedOutputFromInput.csv"))
		// .trim(), outContent.toString().trim());
		Assert.assertTrue(outContent.toString().trim()
				.contains("DRONE_B : 25 18 S"));
		Assert.assertTrue(outContent.toString().trim()
				.contains("DRONE_A : 0 13 O"));
	}

	@Test(timeout = TIMEOUT_MS)
	public void testMapTooBig() {
		String[] args = { "-i", "src/test/resources/inputBigMap.csv", "-t" };
		Application.main(args);
		Assert.assertEquals(
				"Error while parsing file. Length must not be superior to 200 and width 150. You defined 20000 25",
				outContent.toString().trim());
	}

	@Test(timeout = TIMEOUT_MS)
	public void testMapTooNotCorrect() {
		String[] args = { "-i", "src/test/resources/inputMapNotCorrect.csv",
				"-t" };
		Application.main(args);
		Assert.assertEquals(
				"Error while parsing file. could not parse first line of file : a. Excepted two numbers seperated by space.",
				outContent.toString().trim());
	}

	@Test(timeout = TIMEOUT_MS)
	public void testPositionNotCorrect() {
		String[] args = { "-i",
				"src/test/resources/inputPositionNotCorrect.csv", "-t" };
		Application.main(args);
		Assert.assertEquals(
				"Error while parsing file. Error while parsing position : 15 32 . Expected two numbers, a letter (NESO) seperated by space.",
				outContent.toString().trim());
	}

	@Test(timeout = TIMEOUT_MS)
	public void testCommandChainTooLong() {
		String[] args = { "-i",
				"src/test/resources/inputCommandChainTooLong.csv", "-t" };
		Application.main(args);
		Assert.assertEquals(
				"Error while parsing file. You cannot have a command of more than 100 characters. See drone DRONE_B",
				outContent.toString().trim());
	}

}