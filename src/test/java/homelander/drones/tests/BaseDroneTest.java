package homelander.drones.tests;

import homelander.drones.Drone;
import homelander.drones.batch.Application;
import homelander.drones.exceptions.CommandChainTooLongException;
import homelander.drones.exceptions.DroneParseException;
import homelander.drones.position.CartesianCoordinatePosition;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class BaseDroneTest {

	/**
	 * Tests timeout.
	 */
	private static final int TIMEOUT_MS = 100000;

	@Autowired
	@Qualifier("droneBase")
	private Drone droneToTest;


	@Test(timeout = TIMEOUT_MS)
	public void testDroneWithEmptyCommands() throws DroneParseException,
			CommandChainTooLongException {
		CartesianCoordinatePosition.setMaximumXCoord(55);
		CartesianCoordinatePosition.setMaximumYCoord(25);
		droneToTest.init("d", "7 10 N", "");
		droneToTest.run();
		Assert.assertEquals("7 10 N", droneToTest.getPosition().toString());
	}

	@Test(timeout = TIMEOUT_MS)
	public void testDroneWithGivenCommand() throws DroneParseException,
			CommandChainTooLongException {
		CartesianCoordinatePosition.setMaximumXCoord(55);
		CartesianCoordinatePosition.setMaximumYCoord(25);
		droneToTest.init("DRONE_A", "7 10 N", "AAGAAADAAGAAAGADAA");
		droneToTest.run();
		Assert.assertEquals("0 13 O", droneToTest.getPosition().toString());
	}

	@Test(timeout = TIMEOUT_MS)
	public void testDroneWithGivenCommandSecond() throws DroneParseException,
			CommandChainTooLongException {
		String commands = "AAADAAGAAADAAGAAAAAADA";
		CartesianCoordinatePosition.setMaximumXCoord(55);
		CartesianCoordinatePosition.setMaximumYCoord(25);
		droneToTest.init("DRONE_B", "13 23 E", "AAADAAGAAADAAGAAAAAADA");
		droneToTest.run();
		Assert.assertEquals("25 18 S", droneToTest.getPosition().toString());
	}

	@Test(timeout = TIMEOUT_MS, expected = DroneParseException.class)
	public void testDroneWithWrongPosition() throws DroneParseException,
			CommandChainTooLongException {
		String commands = "AAA";

		droneToTest.init("a", "0 0 Q", commands);

		droneToTest.run();
		Assert.assertEquals("25 18 S", droneToTest.getPosition().toString());
	}

	@Test(timeout = TIMEOUT_MS)
	public void testDroneWithWrongCommand() throws DroneParseException,
			CommandChainTooLongException {
		String commands = "AAAZ";
		CartesianCoordinatePosition.setMaximumXCoord(55);
		CartesianCoordinatePosition.setMaximumYCoord(25);

		droneToTest.init("a", "0 0 N", commands);
		droneToTest.run();
		Assert.assertEquals("0 3 N", droneToTest.getPosition().toString());
	}

	@Test(timeout = TIMEOUT_MS, expected = CommandChainTooLongException.class)
	public void testDroneWithMoreHundredCommands() throws DroneParseException,
			CommandChainTooLongException {
		StringBuffer outputBuffer = new StringBuffer(101);
		for (int i = 0; i < 101; i++) {
			outputBuffer.append("A");
		}
		String commands = outputBuffer.toString();
		droneToTest.init("a", "0 0 N", commands);
		droneToTest.run();
	}

	/**
	 * @return the droneToTest
	 */
	public Drone getDroneToTest() {
		return droneToTest;
	}

	/**
	 * @param droneToTest
	 *            the droneToTest to set
	 */
	public void setDroneToTest(Drone droneToTest) {
		this.droneToTest = droneToTest;
	}
}