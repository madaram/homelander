package homelander.drones;

import homelander.drones.exceptions.DroneParseException;
import homelander.drones.position.Position;

/**
 * Drone interface. A drone is initialized with a name, a position and a list of
 * commands as a String. It extends Runnable, to start a drone use the method
 * run. You can get position after a run with getPosition. For position and
 * commands format see specifications.
 */
public interface Drone extends Runnable {

	/**
	 * @return Position
	 */
	public Position getPosition();

	/**
	 * @param doneName
	 *            name of drone, no particular format
	 * @param postion
	 *            position as String
	 * @param commands
	 *            commands to execute by the drone when run method is executed
	 * @throws DroneParseException
	 *             thrown if position and command chain not well formated
	 */
	public void init(String doneName, String postion, String commands) throws DroneParseException;

	/**
	 * @return drone name
	 */
	public String getName();
}
