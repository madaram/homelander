/**
 *
 */
package homelander.drones.batch.reader;

import homelander.drones.Drone;
import homelander.drones.exceptions.DroneParseException;
import homelander.drones.exceptions.MaximumDroneNumberException;

import java.io.IOException;
import java.util.List;

/**
 * Class to read file and build a list of drones.
 */
public interface DroneFileReader {
	/**
	 * Read file and build a list of drones.
	 * 
	 * @param filePath
	 *            input file path.
	 * @return list of drones
	 * @throws DroneParseException
	 *             if error while reading file
	 * @throws IOException
	 *             if problems when accessing file
	 * @throws MaximumDroneNumberException
	 *             if too many drones defined
	 */
	public List<Drone> readFile(String filePath) throws DroneParseException,
	IOException, MaximumDroneNumberException;
}
