/**
 *
 */
package homelander.drones.batch.reader;

import homelander.drones.Drone;
import homelander.drones.batch.Application;
import homelander.drones.exceptions.DroneParseException;
import homelander.drones.exceptions.MaximumDroneNumberException;
import homelander.drones.position.Position;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class DroneFileReaderBase implements DroneFileReader {

	private final static Logger LOGGER = Logger.getLogger(DroneFileReaderBase.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * homelander.drones.batch.reader.DroneFileReader#readFile(java.lang.String)
	 */
	public List<Drone> readFile(String filePath) throws DroneParseException, IOException, MaximumDroneNumberException {
		int droneCount = 0;
		List<Drone> drones = new ArrayList<Drone>();
		FileReader file = null;
		String header = null;

		file = new FileReader(filePath);
		BufferedReader br = new BufferedReader(file);
		String line;
		boolean firstLine = true;
		while ((line = br.readLine()) != null) {
			if (firstLine) {
				firstLine = false;
				header = line;
				Position map = Application.getContextSpring().getBean(
						Position.class);
				map.setBoundaries(header);
			} else {
				Drone newDrone = Application.getContextSpring().getBean(
						Drone.class);
				String[] array = line.split(";");
				try {
					newDrone.init(array[0], array[1], array[2]);

				} catch (ArrayIndexOutOfBoundsException e) {
					br.close();
					throw new DroneParseException(
							"Error while parsing line : "
									+ line
							+ ". Expected three fields seperated by ; character.", e);
				}
				LOGGER.debug(String.format("new Drone : %s",
						newDrone.toString()));
				drones.add(newDrone);
				droneCount++;
				if (droneCount > MaximumDroneNumberException.MAXIMUM_DRONE_NUMBER) {
					br.close();
					throw new MaximumDroneNumberException(String.format(
"Error : you defined more than %s drones.",
							MaximumDroneNumberException.MAXIMUM_DRONE_NUMBER));
				}
			}
		}
		br.close();

		return drones;

	}
}
