/**
 *
 */
package homelander.drones.batch;

import homelander.drones.Drone;
import homelander.drones.batch.processor.DroneProcessor;
import homelander.drones.batch.reader.DroneFileReader;
import homelander.drones.batch.writer.DroneFileWriter;
import homelander.drones.exceptions.DroneParseException;
import homelander.drones.exceptions.MaximumDroneNumberException;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Class that read, lunch the drones and write results.
 */
@Component
public class Batch {
	private final DroneFileReader fileReader;
	private final DroneFileWriter fileWriter;
	private final DroneProcessor processor;

	/**
	 * Build batch.
	 *
	 * @param fileReader
	 *            read file and return a list of drones.
	 * @param fileWriter
	 *            write the result from a list of drone.
	 * @param processor
	 *            run the drones.
	 */
	@Autowired
	public Batch(DroneFileReader fileReader, DroneFileWriter fileWriter,
			@Qualifier("droneProcessorThreadPool") DroneProcessor processor) {
		super();
		this.fileReader = fileReader;
		this.fileWriter = fileWriter;
		this.processor = processor;
	}

	/**
	 * Execute batch, read file, build drones, lunch drones then write drone
	 * result.
	 *
	 * @param inputPath
	 *            input file to read (mandatory).
	 * @param outputPath
	 *            output file to write, if not given write in systout.
	 * @throws DroneParseException
	 *             If error while parsing file.
	 * @return success boolean
	 */
	public boolean execute(String inputPath, String outputPath)
 throws DroneParseException {
		List<Drone> drones;
		try {
			drones = fileReader.readFile(inputPath);
		} catch (IOException e) {
			System.out.println("Error while reading file : " + e.getMessage());
			return false;
		} catch (MaximumDroneNumberException e) {
			System.out.println(e.getMessage());
			return false;
		}
		processor.processDrones(drones);
		try {
			fileWriter.writeDroneResult(outputPath, drones);
		} catch (IOException e) {
			System.out.println("Error while writing file. " + e.getMessage());
		}
		return true;
	}

}
