/**
 *
 */
package homelander.drones.batch.writer;

import homelander.drones.Drone;

import java.io.IOException;
import java.util.List;

/**
 * Write drones to file.
 */
public interface DroneFileWriter {

	/**
	 * @param filePath
	 *            path where to write file. If null write in sysout.
	 * @param drones
	 *            drones to write
	 * @throws IOException
	 *             if problem when writing to file
	 */
	public void writeDroneResult(String filePath, List<Drone> drones) throws IOException;

}
