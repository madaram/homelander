/**
 *
 */
package homelander.drones.batch.writer;

import homelander.drones.Drone;
import homelander.drones.batch.Application;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class DroneFileWriterBase implements DroneFileWriter {

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * homelander.drones.batch.writer.DroneFileWriter#writeDroneResult(java.
	 * util.List)
	 */
	public void writeDroneResult(String filePath, List<Drone> drones)
			throws IOException {

		OutputStreamWriter stream;

		if (filePath != null) {
			File file = new File(filePath);
			if (!file.exists()) {
				file.createNewFile();
			}

			stream = new FileWriter(file.getAbsoluteFile());
		} else {
			stream = new OutputStreamWriter(System.out);
		}
		BufferedWriter bw = new BufferedWriter(stream);
		for (Drone d : drones) {
			bw.write(String.format("%s : %s", d.getName(), d.getPosition().toString()));
			bw.newLine();
		}
		if (!Application.getArguments().hasOption("t")) {
			bw.write(String.format("Elapsed time : %s ms", Application.getElapsedTime()));
			bw.newLine();
		}

		bw.close();
	}

}
