package homelander.drones.batch;

import homelander.drones.exceptions.DroneParseException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * Main class. Parse arguments and process the batch.
 */
@ComponentScan(basePackages = { "homelander" })
public class Application {

	/**
	 * 
	 */
	private static final String APPLICATION_NAME = "drones";

	/** Application starting time. */
	private final static long START_TIME = System.currentTimeMillis();

	/** Spring context. */
	private static ApplicationContext springContext;

	/** Application arguments. */
	private static CommandLine arguments;

	private static Options options;
	static {
		options = new Options();
		options.addOption("i", "inputFile", true, "mandatory input file path");
		options.addOption("o", "outputFile", true, "output file path, if not given, output in console");
		options.addOption("d", "debug", false, "show logs");
		options.addOption(new Option("h", "help", false, "print this message"));
		options.addOption(new Option("t", "noTime", false, "don't write elapsed time"));
	}

	/**
	 * @param args
	 *            show help with -h
	 */
	public static void main(String[] args) {
		// process arguments
		String inputPath = null;
		String outputPath = null;

		try {
			CommandLineParser parser = new BasicParser();
			arguments = parser.parse(options, args);
			if (arguments.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(APPLICATION_NAME, options);
				return;
			}
			if (arguments.hasOption("inputFile")) {
				inputPath = arguments.getOptionValue("inputFile");
			} else {
				System.out.println("You must specify an input file.");
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(APPLICATION_NAME, options);
				return;
			}
			if (arguments.hasOption("outputFile")) {
				outputPath = arguments.getOptionValue("outputFile");
			} else {
				outputPath = null;
			}
			if (!arguments.hasOption("debug")) {
				LogManager.getRootLogger().setLevel(Level.OFF);
			}
		} catch (ParseException exp) {
			System.out.println("Parsing failed. " + exp.getMessage());
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(APPLICATION_NAME, options);
			return;
		}

		// building batch and executing it.
		Batch batch = getContextSpring().getBean(Batch.class);
		try {
			boolean success = batch.execute(inputPath, outputPath);
			if ((outputPath != null) && success) {
				System.out.println("result writen in " + outputPath);
			}
		} catch (DroneParseException e) {
			System.out.println("Error while parsing file. " + e.getMessage());
			return;
		}
	}

	/**
	 * @return elapsed time in ms
	 */
	public static long getElapsedTime() {
		long endTime = System.currentTimeMillis();
		return endTime - START_TIME;
	}

	/**
	 * @return spring context
	 */
	public static ApplicationContext getContextSpring() {
		if (springContext == null) {
			springContext = new AnnotationConfigApplicationContext(Application.class);
		}
		return springContext;
	}

	/**
	 * @return the arguments
	 */
	public static CommandLine getArguments() {
		return arguments;
	}

	/**
	 * @param arguments
	 *            the arguments to set
	 */
	public static void setArguments(CommandLine arguments) {
		Application.arguments = arguments;
	}
}