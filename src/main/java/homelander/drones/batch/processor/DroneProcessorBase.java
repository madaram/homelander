package homelander.drones.batch.processor;

import homelander.drones.Drone;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * The simplest way to start all drones. One by one.
 */
@Component
public class DroneProcessorBase implements DroneProcessor {
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * homelander.drones.batch.processor.DroneProcessor#processDrones(java.util
	 * .List)
	 */
	public void processDrones(List<Drone> drones) {
		for (Drone d : drones) {
			d.run();
		}
	}

}
