/**
 *
 */
package homelander.drones.batch.processor;

import homelander.drones.Drone;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;

/**
 * Implementation with a pool of threads.
 */
@Component
public class DroneProcessorThreadPool implements DroneProcessor {

	/**
	 * The maximum number of threads possible un the pool.
	 */
	private static final int THREAD_NUMBER_LIMIT = 20;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * homelander.drones.batch.processor.DroneProcessor#processDrones(java.util
	 * .List)
	 */
	public void processDrones(List<Drone> drones) {
		ExecutorService executor = Executors.newFixedThreadPool(THREAD_NUMBER_LIMIT);
		for (Drone d : drones) {
			Runnable worker = d;
			executor.execute(worker);
		}
		executor.shutdown();
		try {
			executor.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

}
