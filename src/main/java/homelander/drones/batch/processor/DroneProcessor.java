/**
 *
 */
package homelander.drones.batch.processor;

import homelander.drones.Drone;

import java.util.List;


/**
 * Class that start the different drones.
 */
public interface DroneProcessor {

	/**
	 * Call the run method of all drones.
	 * 
	 * @param drones
	 *            the drones to start
	 */
	public void processDrones(List<Drone> drones);

}
