package homelander.drones.exceptions;

public class DroneParseException extends Exception {

	private static final long serialVersionUID = 6317150750523483075L;

	public DroneParseException() {
	}

	public DroneParseException(String message) {
		super(message);
	}

	public DroneParseException(String message, Throwable cause) {
		super(message, cause);
	}
}