package homelander.drones.exceptions;

public class MaximumDroneNumberException extends Exception {

	private static final long serialVersionUID = 6317150750523483075L;

	public static final int MAXIMUM_DRONE_NUMBER = 10;
	// Parameterless Constructor
	public MaximumDroneNumberException() {
	}

	// Constructor that accepts a message
	public MaximumDroneNumberException(String message) {
		super(message);
	}

	public MaximumDroneNumberException(String message, Throwable cause) {
		super(message, cause);
	}
}