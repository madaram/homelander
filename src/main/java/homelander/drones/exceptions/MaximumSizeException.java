package homelander.drones.exceptions;

public class MaximumSizeException extends DroneParseException {

	private static final long serialVersionUID = 6317150750523483075L;

	public static final int MAXIMUM_X = 150;

	public static final int MAXIMUM_Y = 200;

	// Parameterless Constructor
	public MaximumSizeException() {
	}

	// Constructor that accepts a message
	public MaximumSizeException(String message) {
		super(message);
	}

	public MaximumSizeException(String message, Throwable cause) {
		super(message, cause);
	}
}