package homelander.drones.exceptions;

/**
 * Exception that indicate that a command String is too long.
 */
public class CommandChainTooLongException extends DroneParseException {

	private static final long serialVersionUID = 6317150750523483075L;

	public static final int MAXIMUM_CHARACTER_NUMBER = 100;

	public CommandChainTooLongException() {
	}


	public CommandChainTooLongException(String message) {
		super(message);
	}

	public CommandChainTooLongException(String message, Throwable cause) {
		super(message, cause);
	}
}