package homelander.drones;

import homelander.drones.command.Command;
import homelander.drones.exceptions.CommandChainTooLongException;
import homelander.drones.exceptions.DroneParseException;
import homelander.drones.position.Position;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Basic drone implementation.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DroneBase implements Drone {

	/**
	 * Time to sleep between commands in ms.
	 */
	private static final int SLEEP_TIME_MS_BETWEEN_COMMANDS = 10;

	private final static Logger LOGGER = Logger.getLogger(DroneBase.class);

	/** Commands to execute. */
	private String commands;

	/** Drone name. */
	private String name;

	@Autowired
	/**Drone position.*/
	private Position position;

	/*
	 * (non-Javadoc)
	 *
	 * @see homelander.drones.Drone#init(java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	public void init(String droneName, String positionString, String commands)
			throws DroneParseException {
		name = droneName;
		position.init(positionString);
		this.commands = commands;
		if (commands.length() > CommandChainTooLongException.MAXIMUM_CHARACTER_NUMBER) {
			throw new CommandChainTooLongException(
					"You cannot have a command of more than "
					+ CommandChainTooLongException.MAXIMUM_CHARACTER_NUMBER
 + " characters. See drone " + droneName);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		LOGGER.debug(String.format("%s : my initial position is %s", name,
				position.toString()));
		LOGGER.debug(String.format("%s : reading commands...", name));
		List<Command> commandList = readCommands();
		LOGGER.debug(String.format("%s : reading commands ok", name));
		for (Command command : commandList) {
			LOGGER.debug(String.format("%s : executing command %s", name,
 command.toString()));
			position.changePosition(command);
			try {
				Thread.sleep(SLEEP_TIME_MS_BETWEEN_COMMANDS);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
			LOGGER.debug(String.format("%s : my position is %s", name,
					position.toString()));
		}
		LOGGER.debug(String.format("%s : my final position is %s", name,
				position.toString()));
	}

	/**
	 * Parse commands String to a list of Commands. Throw
	 * IllegalArgumentException if a command is not recognized.
	 *
	 * @return List of commands.
	 */
	private List<Command> readCommands() {
		List<Command> commandList = new ArrayList<Command>();
		if (commands != null) {
			for (char ch : commands.toCharArray()) {
				try {
					Command c = Command.getCommandFromChar(ch);
					commandList.add(c);
				} catch (IllegalArgumentException e) {
					// ignore wrong command
					LOGGER.warn(String.format(
							"Illegal command for drone %s : %s", name, ch));
				}
			}
		}
		return commandList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see homelander.drones.Drone#getPosition()
	 */
	public Position getPosition() {
		return position;
	}

	/**
	 * @param position
	 *            the position to set
	 */
	public void setPosition(Position position) {
		this.position = position;
	}

	/*
	 * (non-Javadoc)
	 * @see homelander.drones.Drone#getName()
	 */
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return String.format("%s;%s;%s", name, position.toString(), commands);
	}


}
