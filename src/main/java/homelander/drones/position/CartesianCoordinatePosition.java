package homelander.drones.position;

import homelander.drones.command.Command;
import homelander.drones.exceptions.DroneParseException;
import homelander.drones.exceptions.MaximumSizeException;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Position in cartesion coordinate.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CartesianCoordinatePosition implements Position {

	private int x;

	private int y;

	private Orientation orientation;

	private static int maximumXCoord = 50;

	private static int maximumYCoord = 50;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * homelander.drones.position.Position#init(homelander.drones.position.Position
	 * )
	 */
	public void init(String position) throws DroneParseException {
		try {
			String[] array = position.split(" ");
			x = Integer.valueOf(array[0]);
			y = Integer.valueOf(array[1]);
			orientation = Orientation.getOrientationFromCode(array[2]);
		} catch (NumberFormatException e) {
			throw new DroneParseException(
					"Error while parsing position : "
							+ position
					+ ". Expected two numbers, a letter (NESO) seperated by space.", e);
		} catch (IllegalArgumentException e) {
			throw new DroneParseException("Error while parsing position : "
 + position + ". " + e.getMessage(), e);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new DroneParseException("Error while parsing position : " + position
					+ ". Expected two numbers, a letter (NESO) seperated by space.", e);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see homelander.drones.position.Position#setBoundaries(java.lang.String)
	 */
	public void setBoundaries(String header) throws DroneParseException,
			MaximumSizeException {
		try {
			String[] array = header.split(" ");
			Integer maxXCoord = Integer.valueOf(array[0]);
			Integer maxYCoord = Integer.valueOf(array[1]);

			if ((maxXCoord > MaximumSizeException.MAXIMUM_X) || (maxYCoord > MaximumSizeException.MAXIMUM_Y)) {
				throw new MaximumSizeException(
						String.format(
						"Length must not be superior to %s and width %s. You defined %s",
						MaximumSizeException.MAXIMUM_Y, MaximumSizeException.MAXIMUM_X, header));
			}
			maximumXCoord = maxXCoord;
			maximumYCoord = maxYCoord;

			// TODO use JAVA 7 notations
		} catch (NumberFormatException e1) {
			throw new DroneParseException(
					"could not parse first line of file : " + header
					+ ". Excepted two numbers seperated by space.", e1);
		} catch (ArrayIndexOutOfBoundsException e2) {
			throw new DroneParseException(
					"could not parse first line of file : " + header
					+ ". Excepted two numbers seperated by space.", e2);
		}

	}

	public void changePosition(Command c) {
		switch (c) {
		case TURN_RIGHT:
			turnRight();
			break;
		case TURN_LEFT:
			turnLeft();
			break;
		case GO_FORWARD:
			goForward();
			break;
		default:
			throw new UnsupportedOperationException("unexpected Command : "
					+ c.toString());
		}
	}

	@Override
	public String toString() {
		return String.format("%s %s %s", x, y, orientation);
	}

	/**
	 * Execute turn right command.
	 */
	private void turnRight() {
		orientation = Orientation.turnRight(orientation);
	}

	/**
	 * Execute turn left command.
	 */
	private void turnLeft() {
		orientation = Orientation.turnLeft(orientation);
	}

	/**
	 * Execute goForward command.
	 */
	private void goForward() {
		switch (orientation) {
		case NORTH:
			if (y < maximumYCoord) {
				y = y + 1;
			}
			break;
		case SOUTH:
			if (y > 0) {
				y = y - 1;
			}
			break;
		case EAST:
			if (x < maximumXCoord) {
				x = x + 1;
			}
			break;
		case WEST:
			if (x > 0) {
				x = x - 1;
			}
			break;
		default:
			throw new IllegalArgumentException("orientation is not correct : "
					+ orientation.toString());
		}
	}

	/**
	 * @param maximumXCoord
	 *            the maximumXCoord to set
	 */
	public static void setMaximumXCoord(int maximumXCoord) {
		CartesianCoordinatePosition.maximumXCoord = maximumXCoord;
	}

	/**
	 * @param maximumYCoord
	 *            the maximumYCoord to set
	 */
	public static void setMaximumYCoord(int maximumYCoord) {
		CartesianCoordinatePosition.maximumYCoord = maximumYCoord;
	}

}
