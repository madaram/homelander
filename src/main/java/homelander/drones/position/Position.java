package homelander.drones.position;

import homelander.drones.command.Command;
import homelander.drones.exceptions.DroneParseException;
import homelander.drones.exceptions.MaximumSizeException;

/**
 * Interface that represent an object position in a map. Can be initizalized
 * with a String.
 */
public interface Position {

	/**
	 * Execute Command and change position.
	 *
	 * @param c
	 *            Command to execute
	 */
	public void changePosition(Command c);

	/**
	 * Parse String position to initialize Position.
	 *
	 * @param position
	 * @throws DroneParseException
	 *             thrown if String position not well formatted.
	 */
	public void init(String position) throws DroneParseException;

	/**
	 * Parse header String to initialize boundaries.
	 *
	 * @param header
	 *            String that give the boundaries of the map.
	 * @throws DroneParseException
	 *             if header format cannot be parsed.
	 * @throws MaximumSizeException
	 *             if boundaries are too big
	 */
	public void setBoundaries(String header) throws DroneParseException,
			MaximumSizeException;

}
