package homelander.drones.position;

/**
 * Enum that represent the 4 cardinal directions.
 */
public enum Orientation {

	NORTH("N"), SOUTH("S"), EAST("E"), WEST("O");

	private String code;

	private Orientation(final String code) {
		this.code = code;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	public static Orientation turnLeft(Orientation orientation) {
		Orientation left = null;
		switch (orientation) {
		case NORTH:
			left = WEST;
			break;
		case SOUTH:
			left = EAST;
			break;
		case EAST:
			left = NORTH;
			break;
		case WEST:
			left = SOUTH;
			break;
		default:
			throw new IllegalArgumentException(
"unknown orientation : " + orientation.getCode());
		}
		return left;
	}

	public static Orientation turnRight(Orientation orientation) {
		Orientation right = null;
		switch (orientation) {
		case NORTH:
			right = EAST;
			break;
		case SOUTH:
			right = WEST;
			break;
		case EAST:
			right = SOUTH;
			break;
		case WEST:
			right = NORTH;
			break;
		default:
			throw new IllegalArgumentException("The character " + orientation.getCode()
					+ " is not a known orientation.");
		}
		return right;
	}

	@Override
	public String toString() {
		return code;
	}

	/**
	 * @param string
	 * @return
	 */
	public static Orientation getOrientationFromCode(String code) {
		for (final Orientation orientation : Orientation.values()) {
			if (orientation.getCode().equals(code)) {
				return orientation;
			}
		}
		throw new IllegalArgumentException("The character " + code
				+ " is not a known orientation.");
	}
}
