package homelander.drones.command;

/**
 * Enum that represent the different commands.
 */
public enum Command {

	TURN_RIGHT("D"), TURN_LEFT("G"), GO_FORWARD("A");

	private String code;

	private Command(final String code) {
		this.code = code;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @param ch
	 *            code
	 * @return corresponding enum or IllegalArgumentException
	 */
	public static Command getCommandFromChar(char code) {
		for (final Command command : Command.values()){
			if (command.getCode().equals(Character.toString(code))) {
				return command;
			}
		}
		throw new IllegalArgumentException("The character " + code + " is not a known command.");
	}


}
