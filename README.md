# HomeLander drones application

This application is an implementation of the HomeLander test. This application has been tested on Windows and Ubuntu.

### Compilation and installation
You need maven (3.0.4 used). Go to root directory where pom.xml is, then execute the command :
```sh
$ mvn clean package appassembler:assemble
```

### Start the application

Once maven command executed, you can start the application with the following command.

```sh
$ ./target/appassembler/bin/drones --help
```

This should start a sh or bat file depending on your OS, you should see :

```sh
usage: drones
 -d,--debug              show logs
 -h,--help               print this message
 -i,--inputFile <arg>    mandatory input file path
 -o,--outputFile <arg>   output file path, if not given, output in console
 -t,--noTime             don't write elapsed time
```

The only mandatory option is the input file (-i or --inputFile), you can test 

```sh
$ ./target/appassembler/bin/drones -i input.csv
DRONE_A : 0 13 O
DRONE_B : 25 18 S
Elapsed time : 546 ms 
```

Have fun !

### Version
1.0.0

### Dependencies
 - spring-context 4.1.5.RELEASE
 - commons-io 2.4
 - log4j 1.2.17
 - commons-cli 1.2
 - junit 4.12
 - spring-test 4.1.5.RELEASE

### Implementation
```
src/
+-- main
¦   +-- java
¦   ¦   +-- homelander
¦   ¦       +-- drones
¦   ¦           +-- DroneBase.java -> main and only drone implementation
¦   ¦           +-- Drone.java  -> drone interface
¦   ¦           +-- batch
¦   ¦           ¦   +-- Application.java -> entry point
¦   ¦           ¦   +-- Batch.java -> read file, run drones and write result
¦   ¦           ¦   +-- processor -> processors run the drones
¦   ¦           ¦   ¦   +-- DroneProcessor.java 
¦   ¦           ¦   ¦   +-- DroneProcessorBase.java
¦   ¦           ¦   ¦   +-- DroneProcessorThreadPool.java
¦   ¦           ¦   +-- reader -> file parsers
¦   ¦           ¦   ¦   +-- DroneFileReader.java
¦   ¦           ¦   ¦   +-- DroneFileReaderBase.java
¦   ¦           ¦   +-- writer -> file writers
¦   ¦           ¦       +-- DroneFileWriter.java
¦   ¦           ¦       +-- DroneFileWriterBase.java
¦   ¦           +-- command -> command enum (ADG)
¦   ¦           ¦   +-- Command.java
¦   ¦           +-- exceptions
¦   ¦           ¦   +-- CommandChainTooLongException.java
¦   ¦           ¦   +-- DroneParseException.java
¦   ¦           ¦   +-- MaximumDroneNumberException.java
¦   ¦           ¦   +-- MaximumSizeException.java
¦   ¦           +-- position -> position objects for the drone
¦   ¦               +-- CartesianCoordinatePosition.java
¦   ¦               +-- Orientation.java
¦   ¦               +-- Position.java
```
